import { Request, Response } from "express"
import { getCustomRepository, IsNull } from "typeorm";
import { resolve } from "path";
import { SurveyRepository } from "../repositories/SurveyRespository";
import { SurveyUsersRepository } from "../repositories/SurveyUserRepository";
import { UserRepository } from "../repositories/UsersRepository";
import SendMailService from "../services/SendMailService";

class SendMailController {
    async execute(request: Request, response: Response) {
        const { email, survey_id } = request.body;

        const userRepository = getCustomRepository(UserRepository);
        const surveyRepository = getCustomRepository(SurveyRepository);
        const surveysUsersRepository = getCustomRepository(SurveyUsersRepository);

        const users = await userRepository.findOne({email});

        if (!users) {
            return response.status(400).json({
                error: "User does not exists"
            });
        }

        const survey = await surveyRepository.findOne({id: survey_id});

        if (!survey) {
            return response.status(400).json({
                error: "Survey does not exists"
            });
        }

        const npsPath = resolve(__dirname, "..", "views", "emails", "npsMail.hbs");

        const surveyUsersAlreadyExists = await surveysUsersRepository.findOne({
            where: {user_id: users.id, value: null},
            relations: ["user", "survey"],
        });

        const variables = {
            name: users.name,
            title: survey.title,
            description: survey.description,
            id: "",
            link: process.env.URL_MAIL,
        };

        if (surveyUsersAlreadyExists) {

            variables.id = surveyUsersAlreadyExists.id;

            await SendMailService.execute(email, survey.title, variables, npsPath);

            return response.json(surveyUsersAlreadyExists);
        }

        const surveyUser = surveysUsersRepository.create({
            user_id: users.id,
            survey_id
        });

        await surveysUsersRepository.save(surveyUser);

        // ENVIAR E-MAIL PARAO USUÁRIO 
        variables.id = surveyUser.id;
        
        await SendMailService.execute(email, survey.title, variables, npsPath);
        
        return response.json(surveyUser);
    }
}

export { SendMailController }