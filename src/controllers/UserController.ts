import { Request, Response } from "express";
import { getCustomRepository } from "typeorm";
import { UserRepository } from "../repositories/UsersRepository";

class UserContoller {
    async create (request: Request, response: Response) {
        const { name, email } = request.body;
        const usersRopository = getCustomRepository(UserRepository);

        const userAlreadyExists = await usersRopository.findOne({
            email
        });

        if (userAlreadyExists) {
            return response.status(400).json({
                error: "User already exists!"
            })
        }

        const user = usersRopository.create({
            name, email
        })

        await usersRopository.save(user);

        return response.status(201).json(user);
    }
}

export { UserContoller };
