Passo a passo para criação de API com express e Typescript

-- INICIANDO O PROJETO
1 - Crie a pasta onde ficara salva a api;

2 - Abra a pasta no VS Code;

3 - Rode o comando 'yarn init -y' para iniciar o yarn no projeto;

4 - Execute o comando 'yarn add express' para instalar o framework express no projeto;

5 - Instale a tipagem do express para ser executado apena no ambiente de desenvolvimento usando o comando 'yarn add @types/express -D';

6 - Instale o typescript no ambiente de desenvolvimento 'yarn add typescript -D'; 

7 - Inicie o type script no projeto para criar o arquivo 'tsconfig.json', para isso rode o comando 'yarn tsc --init';

8 - Dentro do arquivo tsconfig.json, altere a linha 'strict' para 'false';

9 - Nativamente o node não executa o typescript, para isso será preciso converter para javascript em tempo de execução, 
    rode o comando 'yarn add ts-node-dev -D' para instalar um transpilador que detecta arquivos alterados e restarta o projeto 
    e também converte o .ts para .js

10 - Abra o arquivo 'package.json' e adicione a seguinte linha de comando '"scripts": {
    "dev": "ts-node-dev --transpile-only --ignore-watch node_modules src/server.ts"
  },'


11 - PRONTO, EXECUTE O COMANDO 'yarn dev' e caso ocorra algum erro, chore um pouco, vá tomar um café e porfim va pesquisar para entender o que deu errado....


-- INICIANDO TESTES AUTOMATIZADOS

1 - Rode o comando 'yarn add jest @types/jest -D' para instalar as dependencias de teste 

2 - Instale a dependencia 'yarn add ts-jest -D'

3 - Depois de instalado, é necessário configurar o arquivo config do jest, para isso rode o comando 'yarn jest --init'

4 - No arquivo jest.config descomente e mude a linha 'bail: 0' -> 'bail: true' e configure o caminho onde os testes ficaram salvos na linha 'testMatch'

5 - Altere e decomente a linha 'preset: undefined' para 'preset: "ts-jest"'

5 - Crie dentro de src a pasta '__test__' 

6 - Instale a dependecia supertest, para isso rode o comando 'yarn add supertest @types/supertest -D' 

--INSTALANDO DEPENDENCIA DE ENVIO DE EMAIL  

1 - Instale a dependencia 'nodemailer'

2 - Instale a tipagens dos documentos rodando o comando 'yarn add @Types/nodemailer -D3 

3 - Instale a dependencia handlebars paracustomização utilizando html 

Boa sorte e bons estudos!!!!

Created by gabriel.gonzalezbs@gmail.